<?php
if (!defined('VSC_PATH')) {
	define('VSC_PATH', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
}
if (!defined('VSC_RES_PATH')) {
	define('VSC_RES_PATH', VSC_PATH.'res'.DIRECTORY_SEPARATOR);
}
require (VSC_RES_PATH.'config.inc.php');
require (VSC_RES_PATH.'functions.inc.php');

$sVersion = phpversion();
$iMajorVersion = (int)substr($sVersion, 0, 1);
$iMinorVersion = (int)substr($sVersion, 2, 1);
if ($iMajorVersion < 5 || ($iMajorVersion == 5 && $iMinorVersion < 5)) {
	$sMessage = 'libVSC requires PHP version >= 5.5. Your current version is: '.$sVersion;

	throw new ErrorException($sMessage, E_USER_ERROR);
}
